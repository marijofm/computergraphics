﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtmosPlanet : MonoBehaviour {
    private float translacion;
    // Use this for initialization
    void Start () {

        translacion = Time.deltaTime * 15;

    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0f, 15f, 0f) * Time.deltaTime);
        if (Time.time > 20 && Time.time < 40)
        {
            transform.Translate(translacion,0f,0f);
        }

    }
}
