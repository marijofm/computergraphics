﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCode : MonoBehaviour {

    private float zoom = 6f;
    private float smooth = 0.5F;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time >= 35)
        {
            Camera.main.fieldOfView=Mathf.Lerp(Camera.main.fieldOfView, zoom, Time.deltaTime*smooth);
        }
      
    }
}
