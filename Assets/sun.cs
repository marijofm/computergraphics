﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sun : MonoBehaviour {

    private float translacion;
    // Use this for initialization
    void Start()
    {

        translacion = Time.deltaTime * 12;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > 25 && Time.time < 40)
        {
            transform.Translate(0f, translacion, 0f);
        }
    }
}
