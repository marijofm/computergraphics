﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cubo1 : MonoBehaviour {

    public static Renderer r;
    private float timer;
    public Material[] materials;
 

    // Use this for initialization
    void Start () {
        r = GetComponent<Renderer>();
        
        r.material.shader = Shader.Find("Custom/DiffuseShader");
        r.material.SetColor("_Color", Color.black);

        StartCoroutine(Cambio2());
        StartCoroutine(Cambio3());
        StartCoroutine(Cambio4());
        StartCoroutine(Cambio5());
    }

    IEnumerator Cambio2()
    {
        yield return new WaitForSeconds(10);
        r.material=materials[3];
        float mySlider = Mathf.PingPong(Time.time,3);
        r.material.SetFloat("_MySlider", mySlider);
    }

    IEnumerator Cambio3()
    {
        yield return new WaitForSeconds(15);
        r.material = materials[0];
    }

    IEnumerator Cambio4()
    {
        yield return new WaitForSeconds(20);
        r.material = materials[1];
        transform.Rotate(new Vector3(30f, 0f, 0f) * Time.deltaTime);
    }

    IEnumerator Cambio5()
    {
        yield return new WaitForSeconds(30);
        r.material = materials[2];
    }
  

    // Update is called once per frame
    void Update () {

        if (Time.time >= 20)
        {
            transform.Rotate(new Vector3(0f, 15f, 0f) * Time.deltaTime);
        }
      
    }
    
}
