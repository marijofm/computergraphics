﻿Shader "Custom/ScrollingNormalMapShader" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_NormalMap("Normal map", 2D) = "bump" {}
		_NormalMapIntensity("Normal map intensity", Range(0,1)) = 0.5
		_ScrollXSpeed("X Scroll Speed", Range(0,10)) = 2
		_ScrollYSpeed("Y Scroll Speed", Range(0,10)) = 2

	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		struct Input {
		float2 uv_MainTex;
		sampler2D _NormalMap;
		float _NormalMapIntensity;
	};

	sampler2D _MainTex;
	fixed4 _Color;
	fixed _ScrollXSpeed;
	fixed _ScrollYSpeed;

	// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
	// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
	// #pragma instancing_options assumeuniformscaling
	UNITY_INSTANCING_CBUFFER_START(Props)
		// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END

		void surf(Input IN, inout SurfaceOutputStandard o) {
		fixed2 scrolledUV = IN.uv_MainTex;
		fixed xScrollValue = _ScrollXSpeed * _Time;
		fixed yScrollValue = _ScrollYSpeed * _Time;
		scrolledUV += fixed2(xScrollValue, yScrollValue);
		half4 c = tex2D(_MainTex, scrolledUV);
		//o.Albedo = c.rgb * _Color;
		//o.Alpha = c.a;

		o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
		float3 normalMap = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
		normalMap.x *= _NormalMapIntensity;
		normalMap.y *= _NormalMapIntensity;
		o.Normal = normalize(normalMap);
	}
	ENDCG
	}
		FallBack "Diffuse"
}

